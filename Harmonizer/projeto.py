import nltk
import csv
import random

stopwords = nltk.corpus.stopwords.words('portuguese')

entrada = open('entrada.txt', 'r')
texto = entrada.read()
texto = texto.split()

quant = 4
posfrase = 0
frase = []

#letra da música
letra = []

egredy = 0.7

egredySensation = 0.98

#######################Parametros Viterbi#############################
#estados
states = ('resolucao','repouso','tensao')

#evidencia
observation = []

#probabilidade a priori
start_prob = {'resolucao':0.33, 'repouso':0.33, 'tensao':0.33}

#modelo de transição
trans_model = {
	'resolucao' : {'resolucao':0.5,'repouso':0.25,'tensao':0.25},
	'repouso' : {'resolucao':0.25,'repouso':0.5,'tensao':0.25},
	'tensao' : {'resolucao':0.25,'repouso':0.25,'tensao':0.5}
	}

#modelo de transição
sensor_model = {
	'resolucao' : {'Resolucao':0.7,'Repouso':0.15,'Tensao':0.15},
	'repouso' : {'Resolucao':0.2,'Repouso':0.6,'Tensao':0.2},
	'tensao' : {'Resolucao':0.2,'Repouso':0.25,'Tensao':0.55}
	}
###########################Parametros viterbi#######################

#################################### VITERBI #######################
def viterbi(obs, states, start_p, trans_p, emit_p):
    V = [{}]
    path = {}
 
    # Inicializa casos-base (t == 0)
    for y in states:
        V[0][y] = start_p[y] * emit_p[y][obs[0]]
        path[y] = [y]
 
    # Executa Viterbi para t > 0
    for t in range(1, len(obs)):
        V.append({})
        newpath = {}
 
        for y in states:
            (prob, state) = max((V[t-1][y0] * trans_p[y0][y] * emit_p[y][obs[t]], y0) for y0 in states)
            V[t][y] = prob
            newpath[y] = path[state] + [y]
 
        # não se faz necessário lembrar de caminhos antigos
        path = newpath
     
    #print_dptable(V)
    (prob, state) = max((V[t][y], y) for y in states)
    
    return (V, path[state])

#################################### VITERBI #######################


##################Retira Orações sem stopwords da entrada###########
def Oracao(quant):
	i=0
	global letra
	global posfrase
	pal = ""
	while(i < quant and posfrase < len(texto)):
		if(stopwords.count(texto[posfrase]) != 0):
			pal = pal+texto[posfrase]+" "
			posfrase = posfrase + 1
		else:
			frase.append(texto[posfrase])
			pal = pal+texto[posfrase]+" "			
			posfrase = posfrase + 1
			i = i + 1
	letra.append(pal)
	#print(pal)
	return()
##################### Stop words ###################################

################Stemming das Palavras do Dicionario.################
dicionario = []
ProbDici = []
ProbFrase = []
def StemmingDic():
	global dicionario
	global ProbDici
	stemmer = nltk.stem.RSLPStemmer()
	with open('dicionario.csv', newline='') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
		for row in spamreader:
			palavra,Presol,Prep,Pten = row
			dicionario.append(stemmer.stem(palavra))
			ProbDici.append([Presol,Prep,Pten])
	return()
######################### Stemming ###################################

################### Busca no Dicionario e calcula ####################
###################       as probabilidades ##########################
def DiciSearch(FraseSensation):
	ProbFrase = [0,0,0]
	
	for i in range(len(frase)):
		stemmer = nltk.stem.RSLPStemmer()
	
		frase[i] = stemmer.stem(frase[i])
	
		if dicionario.count(frase[i]) > 0:
			index = dicionario.index(frase[i])
			Probs = ProbDici[index]
			
	#Altera a probabilidade da frase caso as sensações
	#das palavras não tenham probabilidades iguais.				
			if(max(Probs) != "0.5"):		
				if(Probs.index(max(Probs)) == 0):
					ProbFrase[0] = ProbFrase[0] + 1
				elif(Probs.index(max(Probs)) == 1):
					ProbFrase[1] = ProbFrase[1] + 1	
				else:	
					ProbFrase[2] = ProbFrase[2] + 1			
		
	#Altera a probabilidade da frase caso as sensações das palavras
	#tenham probabilidades iguais.
			else:
				if(Probs[0] == 0.5 and Probs[1] == 0.5):
					ProbFrase[0] = ProbFrase[0] + 1
					ProbFrase[1] = ProbFrase[1] + 1	

				elif(Probs[1] == 0.5 and Probs[2] == 0.5):

					ProbFrase[1] = ProbFrase[1] + 1	
					ProbFrase[2] = ProbFrase[2] + 1
				else:
					ProbFrase[0] = ProbFrase[0] + 1
					ProbFrase[2] = ProbFrase[2] + 1	


		#Altera probabilidades caso a palavra não esteja no dicionário
		else:
			index = random.choice([0,1,2])
			if(index == 0):
				ProbFrase[0] = ProbFrase[0] + 1
				
			elif(index == 1):
				ProbFrase[1] = ProbFrase[1] + 1	
			else:
				ProbFrase[2] = ProbFrase[2] + 1
	
	
	#print(ProbFrase)
	aux = ProbFrase[0]+ProbFrase[1]+ProbFrase[2]
	ProbFrase[0] = ProbFrase[0]/aux 
	ProbFrase[1] = ProbFrase[1]/aux
	ProbFrase[2] = ProbFrase[2]/aux

	#print(frase)		
	#print(ProbFrase)
	
	index = random.random()
	if(index < egredySensation):
		index = ProbFrase.index(max(ProbFrase))
		if(index == 0):
			FraseSensation = "Resolucao"
		elif(index == 1):
			FraseSensation = "Repouso"
		else:	
			FraseSensation = "Tensao"
	else:
		index = random.choice([0,1,2])
		if(index == 0):
			FraseSensation = "Resolucao"
		elif(index == 1):
			FraseSensation = "Repouso"
		else:	
			FraseSensation = "Tensao"

	return(FraseSensation)

############################# Busca Dicionario####################

########################### Escolhe o Grau #######################
def EscolheGrau(FraseSensation):
	grau = 0
	index = random.random()
	if(FraseSensation == "resolucao"):
		if(index < egredy):
			grau = 1
		else:
			grau = random.choice([1,3,6])
	elif(FraseSensation == "repouso"):
		if(index < egredy):
			grau = 4
		else:
			grau = random.choice([4,2])
	else:
		if(index < egredy):
			grau = 5
		else:
			grau = random.choice([5,7])
	
	return(grau)
######################### Escolhe Grau #############################


######################### GPC #####################################
C = ("C",["C", "D", "E", "F", "G", "A", "B"])
Cs = ("C#",["C#", "D#", "F", "F#", "G#", "A#", "C"])
D =  ("D",["D", "E", "F#", "G", "A", "B", "C#"])
Ds = ("D",["D", "E", "F#", "G", "A", "B", "C#"])
E = ("E", ["E", "F#", "G#", "A", "B", "C#", "D#"])
F = ("F",["F", "G", "A", "A#", "C", "D", "E"])
Fs = ("F#",["F#", "G#", "A#", "B", "C#", "D#", "F"])
G = ("G",["G", "A", "B", "C", "D", "E", "F#"])
Gs = ("G#",["G#", "A#", "C", "C#", "D#", "F", "G"])
A = ("A",["A", "B", "C#", "D", "E", "F#", "G#"])
As = ("A#",["A#", "C", "D", "D#", "F", "G", "A"])
B = ("B",["B", "C#", "D#", "E", "F#", "G#", "A#"])

tons = [C, Cs, D, Ds, E, F, Fs, G, Gs, A, As, B]


def conversaoGpC(tom, Agrau):

    
	menor = ("m", [2,6,3])
	setimoGrau = ("7/5-", [7])
	index = int(Agrau[0])
#index = grau
	for i in tons:
		tomI, sequencia = i
		if(tomI == tom):
			acorde = sequencia[index-1]

	complemento,lista = menor
	if lista.count(index) > 0:
		acorde = acorde + complemento
		return acorde

	complemento,lista = setimoGrau
	if lista.count(index) > 0:
		acorde = acorde + complemento
		return acorde

	return acorde

######################## GPC ######################################

######################### "MAIN" ###################################

StemmingDic()

while(posfrase < len(texto) ):
	FraseSensation = ""
	Oracao(quant)
	FraseSensation = DiciSearch(FraseSensation)
	
	observation.append(FraseSensation)

	grau = EscolheGrau(FraseSensation)

	del frase[:]
	#print (FraseSensation)
	#print (grau)
	#print (conversaoGpC("C",str(grau)))
	#print ("\n")

#print(observation)
v, estado = viterbi(observation, states, start_prob, trans_model, sensor_model)
#print(v)

print('\nSequencia mais provável de acordes',estado)

k = 0
for i in estado:
	grau = EscolheGrau(i)
	print (conversaoGpC("C",str(grau)))
	print(letra[k])
	k = k + 1









