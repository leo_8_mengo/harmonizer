import nltk
import csv

stopwords = nltk.corpus.stopwords.words('portuguese')

entrada = open('entrada.txt', 'r')
texto = entrada.read()
texto = texto.split()

quant = 4
posfrase = 0
frase = []

#Retira Orações sem stopwords da entrada
def Oracao(quant):
	i=0
	global posfrase
	while(i < quant and posfrase < len(texto)):
		if(stopwords.count(texto[posfrase]) != 0):
			posfrase =posfrase + 1
		else:
			frase.append(texto[posfrase])
			posfrase = posfrase + 1
			i = i + 1
	return()


#Stemming das Palavras do Dicionario.
dicionario = []
ProbDici = []
def StemmingDic():
	global dicionario
	global ProbDici
	stemmer = nltk.stem.RSLPStemmer()
	with open('dicionario.csv', newline='') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
		for row in spamreader:
			palavra,Presol,Prep,Pten = row
			dicionario.append(stemmer.stem(palavra))
			ProbDici.append([Presol,Prep,Pten])
	return()

