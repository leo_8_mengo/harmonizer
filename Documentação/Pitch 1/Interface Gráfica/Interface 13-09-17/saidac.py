# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'saidac.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(872, 507)
        Dialog.setAutoFillBackground(False)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(-60, -10, 1021, 631))
        self.label.setStyleSheet("background-image: url(:/newPrefix/bgmain.jpg);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.Entrada_Letra = QtWidgets.QTextEdit(Dialog)
        self.Entrada_Letra.setGeometry(QtCore.QRect(60, 60, 761, 391))
        self.Entrada_Letra.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.Entrada_Letra.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.Entrada_Letra.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContentsOnFirstShow)
        self.Entrada_Letra.setObjectName("Entrada_Letra")
        self.Letra_Analisada = QtWidgets.QLabel(Dialog)
        self.Letra_Analisada.setGeometry(QtCore.QRect(410, 30, 71, 21))
        self.Letra_Analisada.setTextFormat(QtCore.Qt.PlainText)
        self.Letra_Analisada.setWordWrap(False)
        self.Letra_Analisada.setObjectName("Letra_Analisada")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(400, 470, 75, 23))
        self.pushButton.setObjectName("pushButton")

        self.retranslateUi(Dialog)
        self.pushButton.clicked.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.Letra_Analisada.setText(_translate("Dialog", "Música Cifrada"))
        self.pushButton.setText(_translate("Dialog", "Ok"))

import bg_rc
import bgqd_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

